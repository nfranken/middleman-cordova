'use strict';

window.app = angular.module('main', [
  'ngCordova',
  'ui.router',
  'ngAnimate'
  // TODO: load other modules selected during generation
])
.config(function ($stateProvider, $urlRouterProvider) {

  console.log('Allo! Allo from your module: ' + 'main');

  $urlRouterProvider.otherwise('/things');

  // some basic routing
  $stateProvider
    .state('things', {
      url: '/things',
      templateUrl: 'things.html'
    })
    .state('boobs', {
      url: '/boobs',
      templateUrl: 'boobs.html'
    });
  // TODO: do your thing
});