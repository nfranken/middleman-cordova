#
# Vendor
#

#= require ../../bower_components/angular/angular
#= require ../../bower_components/angular-mocks/angular-mocks
#= require ../../bower_components/angular-animate/angular-animate
#= require ../../bower_components/angular-ui-router/release/angular-ui-router.min
#= require ../../bower_components/ng-cordova/dist/ng-cordova.min

#
#  Modules
#

#= require lib/index
#= require lib/main/main

#
#  Controllers
#

#= require_tree ./lib/main/controllers