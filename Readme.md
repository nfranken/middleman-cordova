# Installation
1. `cd middleman`
2. `bower install`
3. `bundle`
4. `middleman build`
5. `cd ..` back to main directory
6. `cordova platform add ios`
7. `cordova platform add android`

# Usage
1. `middleman server` within middleman directory